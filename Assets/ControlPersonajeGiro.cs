﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPersonajeGiro : MonoBehaviour {

	public float giro;				// Aquí le pondremos la velocidad de giro 
	public string inputGiro;		// Eje de teclado a leer
	public float lecturaInput;		// Aquí asignaremos el estado de la input
	public Vector3 ejeGiro;			// Dirección en la que girará (0,1,0 para girar como una persona)
	
	// Update is called once per frame
	void Update ( ) {
		
		lecturaInput = Input.GetAxis ( inputGiro );	// Leemos el estado de la input

		// Avanzamos en el eje de avance, lo que diga la lectura de la input, por la velocidad, en metros/segundo
		transform.Rotate ( ejeGiro*lecturaInput*giro*Time.deltaTime );

	}

}
