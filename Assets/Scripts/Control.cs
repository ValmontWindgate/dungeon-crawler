﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Control : MonoBehaviour {

	public static Control main;
	public GameObject player;
	public int playerClass;

	void Start ( ) {
		main = this;
	}
	
	public void CambiarEscena ( int escena , int puerta ) {
		StartCoroutine ( CorrutinaCambiarEscena ( escena , puerta ) );
	}

	public IEnumerator CorrutinaCambiarEscena ( int escena, int puerta ) {
		SceneManager.LoadScene ( escena );
		yield return new WaitForSeconds ( 1f );
		//Puerta [ ] puertas = GameObject.FindObjectsOfType<Puerta>( );
		//foreach ( Puerta actual in puertas ) {
			/* if ( actual.id == puerta ) {
				player.transform.position = puerta.transform.position;
				player.transform.forward = puerta.transform.forward;
				player.transform.position += player.transform.forward;
			}*/
		//}
	}

}
