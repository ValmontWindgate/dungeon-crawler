﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arco : MonoBehaviour {

	public GameObject prefab;
	
	// Update is called once per frame
	void Update () {
		Instantiate ( prefab , transform.position 
		, transform.rotation );
	}

}
