﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoTrigger : MonoBehaviour {

	public Rigidbody rigidbody;
	public float fuerza = 10;

	void Start ( ) {
		rigidbody.AddRelativeForce ( Vector3.forward * fuerza ,
			ForceMode.Impulse );
	}

	void Update () {
		if ( rigidbody ) transform.forward = rigidbody.velocity;
	}

	void OnTriggerEnter ( Collider other ) {
		Destroy ( rigidbody );
		Destroy ( gameObject , 5 );
	}

}
