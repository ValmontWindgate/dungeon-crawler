﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour {

	public Animator animator;
	public GameObject camara;
	public GameObject objetoDireccion;
	public Vector3 diferencia;
	public Vector3 diferenciaTransformada, diferenciaTransformada2; 
	public int estado;
	public int direccion;
	public int direccionTransformada;
	public float tiempoCambioEstado;
	public float tiempoCambioDireccion;
	
	public void Start ( ) {
		animator = GetComponent<Animator>( );
		camara = Camera.main.gameObject;
		InvokeRepeating ( "CambiarEstado" , tiempoCambioEstado , tiempoCambioEstado );
		InvokeRepeating ( "CambiarDireccion" , tiempoCambioDireccion , tiempoCambioDireccion );
	}

	void Update ( ) {
		Vector3 forwardCamara = camara.transform.position - transform.position;
		forwardCamara = new Vector3 ( forwardCamara.x , 0 , forwardCamara.z );
		transform.forward = forwardCamara;
		CalcularDireccionTransformada ( );
		animator.SetInteger ( "Estado" , estado );
		animator.SetInteger ( "Direccion" , direccionTransformada ); 
		// animator.SetInteger ( "Direccion" , direccion ); 
		objetoDireccion.transform.position = transform.position;
		if ( estado == 0 ) {
			if ( direccion == 0 ) {
				objetoDireccion.transform.forward = Vector3.back;
			}
			if ( direccion == 1 ) {
				objetoDireccion.transform.forward = Vector3.forward;
			}	
			if ( direccion == 2 ) {
				objetoDireccion.transform.forward = Vector3.right;
			}
			if ( direccion == 3 ) {
				objetoDireccion.transform.forward = Vector3.left;
			}
		}
		if ( estado == 1 ) {
			if ( direccion == 0 ) {
				transform.Translate ( Vector3.forward * Time.deltaTime , Space.World );
				objetoDireccion.transform.forward = Vector3.forward;
			}
			if ( direccion == 1 ) {
				transform.Translate ( Vector3.back * Time.deltaTime , Space.World );
				objetoDireccion.transform.forward = Vector3.back;
			}	
			if ( direccion == 2 ) {
				transform.Translate ( Vector3.right * Time.deltaTime , Space.World );
				objetoDireccion.transform.forward = Vector3.right;
			}
			if ( direccion == 3 ) {
				transform.Translate ( Vector3.left * Time.deltaTime , Space.World );
				objetoDireccion.transform.forward = Vector3.left;
			}
		}
		
	}

	public void CambiarEstado ( ) {
		estado = Random.Range ( 0 , 2 );
	}

	public void CambiarDireccion ( ) {
		if ( Physics.CheckSphere ( transform.position + Vector3.up , 1 ) ) {
			if ( direccion == 0 ) direccion = 1;
			else if ( direccion == 1 ) direccion = 0; 
			else if ( direccion == 2 ) direccion = 3;
			else if ( direccion == 3 ) direccion = 2;
		}
		else direccion = Random.Range ( 0 , 4 );
	}

	public void CalcularDireccionTransformada ( ) {
		diferencia = transform.position - camara.transform.position;
		diferenciaTransformada = camara.transform.InverseTransformDirection ( diferencia ); 
		if ( diferenciaTransformada.z > 0 ) {	// Está delante nuestro
			if ( diferenciaTransformada.x > diferenciaTransformada.z ) { // Está a nuestra izquierda
				Debug.Log ( "ESTA A NUESTRA IZQUIERDA" );
			}
			else if ( Mathf.Abs ( diferenciaTransformada.x ) > diferenciaTransformada.z ) { // Está a nuestra derecha
				Debug.Log ( "ESTA A NUESTRA DERECHA" );
			}
			else { // Está completamente delante
				Debug.Log ( "ESTA A DELANTE NUESTRO" );
			}
		}
		else {	// Está detrás nuestro, no hace falta renderizarlo
			Debug.Log ( "ESTA DETRAS NUESTRO" );
		}
		diferenciaTransformada2 = camara.transform.InverseTransformDirection ( objetoDireccion.transform.forward ); 
		if ( diferenciaTransformada2.z > 0 ) {	// Está delante nuestro
			if ( diferenciaTransformada2.x > diferenciaTransformada2.z ) { // Está a nuestra izquierda
				Debug.Log ( "MIRA A LA DERECHA" );
				direccionTransformada = 3;
			}
			else if ( Mathf.Abs ( diferenciaTransformada2.x ) > diferenciaTransformada2.z ) { // Está a nuestra derecha
				Debug.Log ( "MIRA A LA IZQUIERDA" );
				direccionTransformada = 2;
			}
			else { // Está completamente delante
				Debug.Log ( "MIRA ADELANTE" );
				direccionTransformada = 1;
			}
		}
		else {	// Está detrás nuestro, no hace falta renderizarlo
			if ( diferenciaTransformada2.x < diferenciaTransformada2.z ) { // Está a nuestra izquierda
				Debug.Log ( "MIRA A LA IZQUIERDA" );
				direccionTransformada = 2;
			}
			else if ( diferenciaTransformada2.x  > Mathf.Abs ( diferenciaTransformada2.z ) ) { // Está a nuestra derecha
				Debug.Log ( "MIRA A LA DERECHA" );
				direccionTransformada = 3;
			}
			else { // Está completamente delante
				Debug.Log ( "MIRA ATRAS" );
				direccionTransformada = 0;
			}
		}
	}

}
