﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Puerta : MonoBehaviour {

	public int escenaDestino;
	public int idPuerta;
	
	void OnTriggerEnter ( Collider infoAcceso ) {
		if ( infoAcceso.tag != "Player" ) return;
		SceneManager.LoadScene ( escenaDestino );
		// AQUÍ HAY QUE BUSCAR LA PUERTA Y MOVER EL PERSONAJE
		// USANDO UNA CORRUTINA
	}

}
