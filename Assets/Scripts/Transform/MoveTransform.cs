﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveTransform : MonoBehaviour {
	
	public string nombreEje;
	public Vector3 direccion;
	public float velocidad = 2f;

	void Update ( ) {
		transform.Translate ( Input.GetAxis ( nombreEje ) *direccion * velocidad * Time.deltaTime );
	}

}
