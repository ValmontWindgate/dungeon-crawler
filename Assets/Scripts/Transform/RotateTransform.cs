﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RotateTransform : MonoBehaviour {
	
	public float velocidad = 90f;

	void Update ( ) {
		transform.Rotate ( Vector3.up * velocidad * Time.deltaTime );
	}

}
