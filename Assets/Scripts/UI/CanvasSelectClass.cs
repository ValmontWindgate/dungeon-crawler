﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasSelectClass : MonoBehaviour {
	
	public Control Control;

	// Update is called once per frame
	public void SelectClass ( int playerClass ) 
	{
		Control.main.playerClass = playerClass;
	}
}
