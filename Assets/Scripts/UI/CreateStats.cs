﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CreateStats : MonoBehaviour {

	public int strength, dexterity, constitution, intelligence, wisdom, charisma;
	public Text strengthInt, dexterityInt, constitutionInt, intelligenceInt, wisdomInt, charismaInt, maxPointsText;
	public int maxPoints;
	public GameObject classCanvas, statCanvas, startGameButton;
	public int sceneIndex;
	// Use this for initialization
	void Start () {

		ModifyStats();
		startGameButton.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () 
	{
		FinishedStats();
	}

	public void ShowStatCanvas()
	{
		classCanvas.SetActive(false);
		statCanvas.SetActive(true);
	}

	public void ModifyStats()
	{
		strengthInt.text = strength.ToString();
		dexterityInt.text = dexterity.ToString();
		constitutionInt.text = constitution.ToString();
		intelligenceInt.text = intelligence.ToString();
		wisdomInt.text = wisdom.ToString();
		charismaInt.text = charisma.ToString();
		maxPointsText.text = maxPoints.ToString();
	}

	public void StrUp()
	{
		if (strength < 10 && maxPoints > 0){ strength ++; maxPoints --;}
		ModifyStats();	
	}
	public void DexUp()
	{
		if (dexterity < 10 && maxPoints > 0){ dexterity ++; maxPoints --;}
		ModifyStats();
	}
	public void ConUp()
	{
		if (constitution < 10 && maxPoints > 0){ constitution ++; maxPoints --;}
		ModifyStats();
	}
	public void IntUp()
	{
		if (intelligence < 10 && maxPoints > 0){ intelligence ++; maxPoints --;}
		ModifyStats();
	}
	public void WisUp()
	{
		if (wisdom < 10 && maxPoints > 0){ wisdom ++; maxPoints --;}
		ModifyStats();
	}
	public void ChaUp()
	{
		if (charisma < 10 && maxPoints > 0){ charisma ++; maxPoints --;}
		ModifyStats();
	}
	public void StrDown()
	{
		if (strength > 1){ strength --; maxPoints ++;}
		ModifyStats();
	}
	public void DexDown()
	{
		if (dexterity > 1){ dexterity --; maxPoints ++;}
		ModifyStats();
	}
	public void ConDown()
	{
		if (constitution > 1){ constitution --; maxPoints ++;}
		ModifyStats();
	}
	public void IntDown()
	{
		if (intelligence > 1){ intelligence --; maxPoints ++;}
		ModifyStats();
	}
	public void WisDown()
	{
		if (wisdom > 1){ wisdom --; maxPoints ++;}
		ModifyStats();
	}
	public void ChaDown()
	{
		if (charisma > 1){ charisma --; maxPoints ++;}
		ModifyStats();
	}

	public void FinishedStats()
	{
		if (maxPoints < 1)
		{
			startGameButton.SetActive(true);
		} else {
			startGameButton.SetActive(false);
		}
	}

	public void StartButton()
	{
		statCanvas.SetActive(false);
		SceneManager.LoadScene(sceneIndex);
	}



}
