using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using VRStandardAssets.Utils;

namespace VRStandardAssets.Menu
{
    // This script makes an UI Button can be used for the VR camera raycaster,
    // if you attach this script to a normal UI Button, this adds 
    // a BoxCollider and configure other components so you can click / long click looking at
    // the button in VR and the button will run its OnClick event
    public class UIButtonToVR : MonoBehaviour {

        private Button button;
        private BoxCollider boxCollider;
        public bool onlyLongClick = false;
        public event Action<UIButtonToVR> OnButtonSelected;                   // This event is triggered when the selection of the button has finished.

        private VRCameraFade m_CameraFade;                 // This fades the scene out when a new scene is about to be loaded.
        private SelectionRadial m_SelectionRadial;         // This controls when the selection is complete.
        private VRInteractiveItem m_InteractiveItem;       // The interactive item for where the user should click to load the level.

        private bool m_GazeOver;                                            // Whether the user is looking at the VRInteractiveItem currently.

        void Awake ( ) {
            // Auto configure components and accesses to make the Button works with camera VR raycaster
            button = gameObject.GetComponent<Button>( );
            if ( button == null ) Debug.LogError ( "This script must be added to a UI normal Button with a Button component!" );
            m_CameraFade = FindObjectOfType<VRCameraFade>( );
            m_SelectionRadial = FindObjectOfType<SelectionRadial>( );
            m_InteractiveItem = gameObject.AddComponent<VRInteractiveItem>( );
            if ( m_CameraFade == null || m_SelectionRadial == null || m_InteractiveItem == null )
                Debug.LogError ( "There is no VR Camera prefab from VR Samples package in the scene!" );
            boxCollider = gameObject.AddComponent<BoxCollider>( );
            RectTransform rectTransform = gameObject.GetComponent<RectTransform>( );
            boxCollider.size = new Vector3 ( rectTransform.rect.width , rectTransform.rect.height , 10 );
        }

        private void OnEnable (){
            m_InteractiveItem.OnOver += HandleOver;
            m_InteractiveItem.OnOut += HandleOut;
            if ( !onlyLongClick ) m_InteractiveItem.OnClick += HandleSelectionComplete;
            if ( onlyLongClick ) m_SelectionRadial.OnSelectionComplete += HandleSelectionComplete;
        }


        private void OnDisable (){
            m_InteractiveItem.OnOver -= HandleOver;
            m_InteractiveItem.OnOut -= HandleOut;
            if ( !onlyLongClick ) m_InteractiveItem.OnClick -= HandleSelectionComplete;
            if ( onlyLongClick ) m_SelectionRadial.OnSelectionComplete -= HandleSelectionComplete;
        }
        

        private void HandleOver(){
            // When the user looks at the rendering of the scene, show the radial.
            m_SelectionRadial.Show();
            m_GazeOver = true;
        }


        private void HandleOut(){
            // When the user looks away from the rendering of the scene, hide the radial.
            m_SelectionRadial.Hide();
            m_GazeOver = false;
        }


        private void HandleSelectionComplete(){
            // If the user is looking at the rendering of the scene when the radial's selection finishes, activate the button.
            if ( m_GazeOver ) StartCoroutine ( ActivateButton ( ) );
        }


        private IEnumerator ActivateButton(){
            // If the camera is already fading, ignore.
            if (m_CameraFade.IsFading)
                yield break;

            // If anything is subscribed to the OnButtonSelected event, call it.
            if (OnButtonSelected != null)
                OnButtonSelected(this);

            // Invoke whatever the UI Button has to do
            button.onClick.Invoke ( );
            Debug.Log ( "UI Button has been watched and longpressed OK from VR camera raycaster!" );

            boxCollider.enabled = false;
            yield return null;
            boxCollider.enabled = true;

        }
    }
}